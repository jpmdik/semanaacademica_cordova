$(document).ready(function(){
    var allowRemove = 0;

    function logout(){
        $.ajax({
            url: URL_API+'/logout',
            //Ajax events
            success: function (data, status, xhr) {
                storage.removeItem("logued");
                window.location.href = 'index.html';
            },
            error: function (e) {
                if(e.status == 401)
                M.toast({html:'Ocorreu algum problema com a solicitacao.', displayLength:4000});
            },
            // Form data
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8',
            type: 'POST',
            xhrFields: { withCredentials: true },
            crossDomain: true,
            //Options to tell jQuery not to process data or worry about content-type.
            cache: true,
            contentType: false,
            processData: false
        });
    }
    
    function remover(id){
        $.ajax({
            url: URL_API+'/remove_contact',
            //Ajax events
            success: function (res, status, xhr) {
                loader(false);
                window.location.href = "home.html";
            },
            error: function (e) {
                if(e.status == 411)
                    M.toast({html: e.responseJSON.msg, displayLength:4000});
                loader(false);
            },
            // Form data
            data: JSON.stringify({"id": id}),
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8',
            type: 'DELETE',
            xhrFields: { withCredentials: true },
            crossDomain: true,
            //Options to tell jQuery not to process data or worry about content-type.
            cache: true,
            contentType: false,
            processData: false
        });
    }

    $(document).on("click", '#remover', function () {
        if(allowRemove == 0){
            allowRemove++;
            M.toast({html: "Clique duas vezes para remover", displayLength:2000});

            setTimeout(function () { 
                allowRemove = 0;
            }, 500);
        }
        else if(allowRemove == 1){
            remover(parseInt($(this).val()));
            allowRemove = 0;
        }
    });

    $('#logout').click(function(){
        logout();
    });
    
    if(Boolean(storage.getItem("logued")) != true){
        window.location.href = 'index.html';
    }
});