URL_API = "http://127.0.0.1:5000"

var storage = window.localStorage;

function loader(status){
    status == true ? $("#loader").removeClass("hide") : $("#loader").addClass("hide")
}

$(document).ready(function(){
    $('.sidenav').sidenav();
    $('.tabs').tabs();
    $('.modal').modal();
});

function obterTipos() {
    var result = null;
    $.ajax({
        url: URL_API + "/tipo",
        method: 'GET',
        dataType: 'json',
        async: false,
        success: function(data, status, xhr){
            result = data.tipos;
        },
        error: function (request, status, error) {
            storage.removeItem("logued");
            window.location.href = "index.html"
        }
    });
    
    return result;
}

function obterContatos(tipo){
    var result = null;
    $.ajax({
        url: URL_API + "/get_contatos",
        method: 'GET',
        data: 'tipo='+tipo,
        async: false,
        xhrFields: { withCredentials: true },
        crossDomain: true,
        success: function(data, status, xhr){
            result = data.contatos;
        },
        error: function (request, status, error) {
            result = undefined;
        }
    });
    
    return result;
}

function obterDadosUser() {
    var result = null;
    $.ajax({
        url: URL_API + "/get_user_data",
        method: 'GET',
        dataType: 'json',
        async: false,
        xhrFields: { withCredentials: true },
        crossDomain: true,
        success: function(data, status, xhr){
            result = data;
        },
        error: function (request, status, error) {
            result = undefined;
        }
    });
    
    return result;
}