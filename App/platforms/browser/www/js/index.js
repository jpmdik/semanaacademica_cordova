$(document).ready(function(){
    
    $('.modal').modal();
    
    $("#email").focus();
    
    $('#email').keydown(function(e){
        if (e.keyCode == 13) {
            $("#password").focus();
        }
    });
    
    
    $('#password').keydown(function(e){
        if (e.keyCode == 13) {
            $("#conect").trigger( "click" );
        }
    });
    
    $("#conect").click(function(){
        loader(true);
        dados = new Object();
        dados.username = $("#email").val();
        dados.password = $("#password").val();
        login(dados);
    });
    
    function login(dados){
        $.ajax({
			url: URL_API+'/auth',
            //Ajax events
            success: function (data, status, xhr) {
                loader(false);
                storage.setItem("logued", true);
                window.location.href = 'home.html';
            },
            error: function (e) {
                if(e.status == 401)
                    M.toast({html:'Usuário ou senha inválido.', displayLength:4000});
                if(e.status == 0)
                    M.toast({html:'Serviço temporariamente indisponível.', displayLength:4000});
                loader(false);
                $("#email").focus();
                $('#password').val("");
            },
			// Form data
            data: JSON.stringify(dados),
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8',
            type: 'POST',
            xhrFields: { withCredentials: true },
            crossDomain: true,
			//Options to tell jQuery not to process data or worry about content-type.
			cache: true,
			contentType: false,
			processData: false
        });
    }
    
    function loader(status){
        status == true ? $("#loader").removeClass("hide") : $("#loader").addClass("hide")
    }

    if(Boolean(storage.getItem("logued")) == true){
        window.location.href = 'home.html';
    }
});