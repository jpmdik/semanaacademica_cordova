$(document).ready(function(){
    $('#email').keydown(function(e){
        if (e.keyCode == 13) {
            $("#senha").focus();
        }
    });
    
    $('#senha').keydown(function(e){
        if (e.keyCode == 13) {
            $("#alterar").trigger( "click" );
        }
    });
    
    function editar(dados){
        $.ajax({
			url: URL_API+'/edit',
            //Ajax events
            success: function (res, status, xhr) {
                loader(false);
                window.location.href = "home.html";
            },
            error: function (e) {
                if(e.status == 401)
                    M.toast({html:'Email já existe.', displayLength:4000});
                else if(e.status == 411)
                    M.toast({html: e.responseJSON.msg, displayLength:4000});
                loader(false);
            },
			// Form data
            data: JSON.stringify(dados),
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8',
            type: 'POST',
            xhrFields: { withCredentials: true },
            crossDomain: true,
			//Options to tell jQuery not to process data or worry about content-type.
			cache: true,
			contentType: false,
			processData: false
        });
    }
    
    $("#alterar").click(function () {
        loader(true);

        dados = new Object();
        dados.email = $("#email").val();
        dados.senha = $("#senha").val();
        editar(dados);
    });
});