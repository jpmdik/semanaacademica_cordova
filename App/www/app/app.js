// Define the `poupeMaisApp` module
var agendaApp = angular.module('agendaApp', []);

agendaApp.controller('criarContatoController', function criarContatoController($scope) {    
    $scope.tipos = obterTipos();
});

agendaApp.controller('editarUserController', function editarUserController($scope) {    
    $scope.data = obterDadosUser();
});

agendaApp.controller('contatosController', function contatosController($scope) {    
    $scope.user = obterDadosUser();

    $scope.pessoal = obterContatos(1);

    console.log($scope.user);

    $scope.empresarial = obterContatos(2);
});