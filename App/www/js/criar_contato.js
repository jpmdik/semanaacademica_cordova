$(document).ready(function(){
    
    $('#telefone').keydown(function(e){
        if (e.keyCode == 13) {
            $("#nome").focus();
        }
    });

    $('#nome').keydown(function(e){
        if (e.keyCode == 13) {
            $("#email").focus();
        }
    });
    
    $('#email').keydown(function(e){
        if (e.keyCode == 13) {
            $("#gravar").trigger( "click" );
        }
    });
    
    function gravar(dados){
        $.ajax({
			url: URL_API+'/save_contact',
            //Ajax events
            success: function (res, status, xhr) {
                loader(false);
                M.toast({html:'Cadastrado com sucesso!', displayLength:1500, completeCallback: function(){ window.location.href = "home.html"}});
            },
            error: function (e) {
                loader(false);
            },
			// Form data
            data: JSON.stringify(dados),
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8',
            type: 'POST',
            xhrFields: { withCredentials: true },
            crossDomain: true,
			//Options to tell jQuery not to process data or worry about content-type.
			cache: true,
			contentType: false,
			processData: false
        });
    }
    
    $("#gravar").click(function () {
        loader(true);

        dados = new Object();
        dados.telefone = $("#telefone").val();
        dados.nome = $("#nome").val();
        dados.email = $("#email").val();
        dados.tipo = $('select#tipo').val().split(":")[1]

        gravar(dados);
    });

    $("#telefone").mask("(99)99999-9999");

    $('select').formSelect();
});